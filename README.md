Worldwind release 1.5.1 and 2.0 (daily build - 11/09/2013) are projects originally based on ANT that I downloaded from  
http://worldwind.arc.nasa.gov/java/. 

I converted these ANT projects to maven to facilitate the integration of the sdk to any maven projects.
I just did the conversion to Maven and I didn't make any changes to the original Worldwind sdk code. I'm sharing this 
implementation to the worldwind community. This bitbucket  repository has public access. I hope you find this conversion 
to maven useful for your implementations based of Worldwind. 


Worldwind1.5.1.0MavenizedParent - this maven project create poms for all the worldwind dependencies and publish them 
into a Nexus server. Do a clean install of this project before doing a clean install of the following
worldwind and worldwindx projects. You must modify the parent pom and change the Nexus URL. I'm publishing
the dependency poms to the Nexus SNAPSHOT repository. To publish to the release repository just remove the string -SNAPSHOT 
from the project version.

Worldwind1.5.1.0MavenizedParent/Worldwind - This maven project builds the worldwind jar version 1.5.1 and corresponding pom. 
The pom is published into the local .m2 directory

Worldwind1.5.1.0MavenizedParent/Worldwindx - This maven project builds the worldwindx jar version 1.5.1 and corresponding pom. 
The pom is published into the local .m2 directory


Worldwind2.0MavenizedParent - this maven project create poms for all the worldwind dependencies and publish them 
into a Nexus server. Do a clean install of this project before doing a cleqan install of the following
worldwind and worldwindx projects.  You must modify the parent pom and change the Nexus URL. I'm publishing
the dependency poms to the Nexus SNAPSHOT repository. To publish to the release repository just remove the string -SNAPSHOT 
from the project version. 

Worldwind2.0MavenizedParent/Worldwind - This maven project builds the worldwind jar version 2.0 and corresponding pom. 
The pom is published into the local .m2 directory. Go to http://worldwind.arc.nasa.gov/java/ to get the latest 2.0 daily
build or the release version and replace the source located in the src/main/java folder.

Worldwind2.0MavenizedParent/Worldwindx - This maven project builds the worldwindx jar version 2.0 and corresponding pom. 
The pom is published into the local .m2 directory. Go to http://worldwind.arc.nasa.gov/java/ to get the latest 2.0 daily
build or the release version and replace the source  located in the src/main/java folder.





What I used to compile and publish the poms:

Tomcat 7.0.41
Nexus-2.6.4-02 - Deploy the Nexus war in Tomcat webapps folder.
SourceTree 1.3.3.0 (Using BitBucket GIT repository)
Netbeans 7.4 (includes maven 3.0.4)
JDK 1.7.0_45



